package tema1poo;

 

public class Cerc extends Forma{
    private Punct centru;
    private Double raza;
    private Double arie;
    private Double perimetru;
    
    public Cerc(Double x, Double y,Double raza){
        centru = new Punct(x,y);
        this.raza = raza;
    }

    @Override
    public String toString(){
        return String.format("cerc %.2f %.2f", arie, perimetru);
    }
    
    @Override
    public void arie() {
        arie=(Double)3.14*raza*raza;
    }

    @Override
    public void perimetru() {
        perimetru=(double)(2.0*3.14*raza);
    }
    
    @Override
    public double getArie() {
        return arie;
    }

    @Override
    public double getPerimetru() {
        return perimetru;
    }
    
    public boolean isTangent(Cerc c){
        double d = c.centru.getDistance(c.centru);
        if(Math.abs(raza-c.raza)<d)
            return true;
        else
            return false;
    }
}
