package tema1poo;

 

public class Dreptunghi extends Forma {
    private Punct StangaSus;
    private Punct DreaptaJos;
    private double arie;
    private double perimetru;
    public Dreptunghi(double xStangaSus,double yStangaSus,double xDreaptaJos, double yDreaptaJos)
    {
        StangaSus = new Punct(xStangaSus,yStangaSus);
        DreaptaJos = new Punct(xDreaptaJos,yDreaptaJos);
    }

    @Override
    public String toString(){
        return String.format("dreptunghi %.2f %.2f", arie, perimetru);
    }

    
    @Override
    public double getArie() {
        return arie;
    }

    @Override
    public double getPerimetru() {
        return perimetru;
    }

    @Override
    public void arie() {
        double L = (double)Math.abs(DreaptaJos.getX()-StangaSus.getX());
        double l = (double)Math.abs(DreaptaJos.getY()-StangaSus.getY());
        arie = (double)(L * l);
    }

    @Override
    public void perimetru() {
        double L = (double)Math.abs(DreaptaJos.getX()-StangaSus.getX());
        double l = (double)Math.abs(DreaptaJos.getY()-StangaSus.getY());
        perimetru = (double) (2.0 * L + 2.0 * l);
    }
}
