package tema1poo;



public abstract class Forma {
    public abstract void arie();
    public abstract void perimetru();
    public abstract double   getArie();
    public abstract double getPerimetru();
}
