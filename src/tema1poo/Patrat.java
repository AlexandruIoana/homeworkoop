package tema1poo;

public class Patrat extends Forma{
    private Punct StangaSus;
    private Punct DreaptaJos;
    private double arie;
    private double perimetru;
    
    public Patrat(double xStangaSus,double yStangaSus,double xDreaptaJos, double yDreaptaJos)
    {
        StangaSus = new Punct(xStangaSus,yStangaSus);
        DreaptaJos = new Punct(xDreaptaJos,yDreaptaJos);
    }
    
    @Override
    public String toString(){
        return String.format("patrat %.2f %.2f", arie, perimetru);
    }
     
    @Override
    public double getArie() {
        return arie;
    }

    @Override
    public double getPerimetru() {
        return perimetru;
    }

    @Override
    public void arie() {
        double l = Math.abs(DreaptaJos.getX()-StangaSus.getX());
        arie = l*l;
    }

    @Override
    public void perimetru() {
        double l = Math.abs(DreaptaJos.getX()-StangaSus.getX());
        perimetru =(double)(l + l + l + l);
    }
}
