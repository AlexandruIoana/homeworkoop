package tema1poo;

public class Punct {
    private double x;
    private double y;
    
    
    
    public Punct(double x, double y){
        this.x = x;
        this.y = y;
    }
    
    public double getX(){
        return this.x;
    }
    
    public double getY(){
        return this.y;
    }
    
    public double getDistance(Punct p){
        return (double)Math.sqrt((x-p.getX()*(x-p.getX())+(y-p.getY())*(y-p.getY())));
    }
}
