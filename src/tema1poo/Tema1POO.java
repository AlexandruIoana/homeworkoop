package tema1poo;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class Tema1POO {

    public static void main(String[] args) {
        int n,i,j;
        double min,max;
        String line;
        String[] strs;
        
        DecimalFormat df = new DecimalFormat("#.####");
        
        Scanner input = new Scanner(System.in);
        line = input.nextLine();
        strs = line.trim().split("\\s+");
        
        // pt a citi n min si max se introduc pe o singura linie cele 3 valori
        n = Integer.parseInt(strs[0]);
        min = Double.parseDouble(df.format(Double.parseDouble(strs[1])));
        max = Double.parseDouble(df.format(Double.parseDouble(strs[2])));
        
        if(n < 1 || n > 500){
            System.out.println("Valoare de intrare invalida");
            return;
        }
        
        // salvez intr-un Arraylist, toate formele care sunt introduse de la tastatura
        // pentru a citi de la tastatura o forma de tip patrat, sau dreptungi se introduce de ex. 'dreptunghi 40 23 45 30.5', iar pt cerc se introduce de ex. 'cerc 10 21 17'
        ArrayList<Object> list = new ArrayList<Object>();
        for(i=1;i<=n;i++){
            line = input.nextLine();
            strs = line.trim().split("\\s+");
            if(strs[0].toLowerCase().equals("dreptunghi")){
                //verificare conditii  pentru coordonate, daca se afla in intervalul de definitie
                Double xStangaSus = Double.parseDouble(df.format(Double.parseDouble(strs[1])));
                Double yStangaSus = Double.parseDouble(df.format(Double.parseDouble(strs[2])));
                Double xDreaptaJos = Double.parseDouble(df.format(Double.parseDouble(strs[3])));
                Double yDreaptaJos = Double.parseDouble(df.format(Double.parseDouble(strs[4])));
                if((xStangaSus>=-1000.000 && xStangaSus<=1000.000) && (yStangaSus>=-1000.000 && yStangaSus<=1000.000) && (xDreaptaJos>=-1000.000 && xDreaptaJos<=1000.000) && (yDreaptaJos>=-1000.000 && yDreaptaJos<=1000.000))
                    list.add(new Dreptunghi(xStangaSus , yStangaSus , xDreaptaJos ,yDreaptaJos));
                else{
                    System.out.println("Valoare de intrare invalida");
                    return;
                }
            }
            else{
                if(strs[0].toLowerCase().equals("patrat")){
                    Double xStangaSus = Double.parseDouble(df.format(Double.parseDouble(strs[1])));
                    Double yStangaSus = Double.parseDouble(df.format(Double.parseDouble(strs[2])));
                    Double xDreaptaJos = Double.parseDouble(df.format(Double.parseDouble(strs[3])));
                    Double yDreaptaJos = Double.parseDouble(df.format(Double.parseDouble(strs[4])));
                    //verificare conditii  pentru coordonate, daca se afla in intervalul de definitie
                    if((xStangaSus>=-1000.00 && xStangaSus<=1000.00) && (yStangaSus>=-1000.00 && yStangaSus<=1000.00) && (xDreaptaJos>=-1000.000 && xDreaptaJos<=1000.000) && (yDreaptaJos>=-1000.000 && yDreaptaJos<=1000.000))
                        list.add(new Patrat(xStangaSus , yStangaSus , xDreaptaJos ,yDreaptaJos));
                    else{
                        System.out.println("Valoare de intrare invalida");
                        return;
                    }
                }
                else{
                    if(strs[0].toLowerCase().equals("cerc")){
                        Double xCentru = Double.parseDouble(df.format(Double.parseDouble(strs[1])));
                        Double yCentru = Double.parseDouble(df.format(Double.parseDouble(strs[2])));
                        Double raza = Double.parseDouble(df.format(Double.parseDouble(strs[3])));
                        // verificare conditii pentru raza si coordonate centru, daca se afla in intervalul de definitie
                        if((raza>0.000 && raza<=1000.000) &&(xCentru>=-1000.000 && yCentru<=1000.000))
                            list.add(new Cerc(xCentru , yCentru , raza));
                        else{
                            System.out.println("Valoare de intrare invalida");
                            return;
                        }
                    }
                }
            }
        }
        
        
        //determinare arie si perimetru
        for(Object o : list){
            ((Forma) o).arie();
            ((Forma) o).perimetru();
        }
        
        System.out.println("Inainte de sortare");
        for(Object o : list){
            System.out.print(o+"\n");
        }
         
        // am folosit ca metoda de sortare Selection sort
        for(i=0;i<list.size()-1;i++){
            int index = i;
            for(j=i+1;j<list.size();j++)
                if( ((Forma) list.get(j)).getArie() < ((Forma) list.get(index)).getArie() )
                    index = j;
                else
                    // in caz de egalitate pt arii, se verifica perimetrele
                    if( Math.abs(1 - ((Forma) list.get(index)).getArie()/((Forma) list.get(j)).getArie())< 0.0001 )
                        if( ((Forma) list.get(j)).getPerimetru() < ((Forma) list.get(index)).getPerimetru() )
                            index = j;
            Object aux = list.get(index);
            list.set(index, list.get(i));
            list.set(i, aux);
        }
        
        int cnt = 0;
        //printare forme ordonate si determinare numar forme care au aria intre min si max
        System.out.println("Dupa sortare");
        for(Object o : list){
            System.out.print(o+"\n");
            if(((Forma) o).getArie()<=max && ((Forma) o).getArie()>=min)
                cnt++;
        }
    
        
        //determinare daca doua cercuri se suprapun, daca a gasit doua cercuri care se suprapu, se opreste cautarea
        Boolean ok = false;
        for(i=0;i<list.size()-1;i++){
            if(list.get(i) instanceof Cerc){
                for(j=i+1;j<list.size();j++){
                    if(list.get(j) instanceof Cerc)
                        if(((Cerc)list.get(i)).isTangent((Cerc)list.get(j))==true){
                            ok = true;
                            System.out.println("da");
                            break;
                        }
                }
                if(ok==true)
                    break;
            }
        }
        if(ok == false)
            System.out.println("nu");
    }
}



